package com.ds.programs;

public class CustomLinkedList {

	class Node{
		int data;
		Node next;
		
		public Node(int data) {
			this.data = data;
		}
	}
	
	private Node head;
	
	public CustomLinkedList() {
		this.head = null;
	}
	
	public void insert(int data) {
		Node newNode = new Node(data);
		if(this.head == null) {
			this.head = newNode;
		}else {
			Node currentNode = head;
			while(currentNode.next !=null) {
				currentNode = currentNode.next;
			}
			currentNode.next = newNode;
		}
		
	}
	 public void insertAt(int index, int data) {
		 Node node=head;
		 Node nodeToBeInserted = new Node(data);
		 for(int i =0 ; i < index-1; i ++) {
			 node = node.next;
		 }
		 nodeToBeInserted.next = node.next;
		 node.next = nodeToBeInserted;
	 }
	 
	 public void removeAt(int index, int data) {
		 Node node=head;
		 for(int i =0 ; i < index-1; i ++) {
			 node = node.next;
		 }
		 node.next = node.next.next;
	 }
	 
	 public void insertHead(int data) {
		 Node node = new Node(data);
		 node.next = head;
		 head =node;
	 }
	 
	 
	 public void display() {
		 if(head !=null) {
			 Node node= head;
			 while(node.next !=null) {
				 System.out.println(node.data);
				 node = node.next;
			 }
			 System.out.println(node.data);
		 }
	 }
	 
	 public Node reverse() {
		 if(head != null) {
			 Node prev= null;
			 Node current = head;
			 Node next ;
			 while(current != null) {
				next= current.next;
				current.next = prev;
				prev=  current;
				current= next;
			 }
			 
			 return prev;
		 }
		 return null;
		 
	 }
	
	public static void main(String[] args) {
		CustomLinkedList cls= new CustomLinkedList();
		cls.insert(1);
		cls.insert(4);
		cls.insertAt(1, 5);
		cls.display();
		//cls.removeAt(1, 5);
		System.out.println("Insert @head");
		cls.insertHead(6);
		cls.display();
		System.out.println("Reverse");
		Node rev = cls.reverse();
		while(rev.next !=null) {
			System.out.println(rev.data);
			rev = rev.next;
		}
		System.out.println(rev.data);
		
	}
}

