public class BinarySearchAlgorithm {

	Node root;

	public BinarySearchAlgorithm() {
		root = null;
	}

	public void insert(int data) {
		root = insertData(root, data);
	}

	private Node insertData(Node root, int data) {
		if (root == null) {
			root = new Node(data);
		} else if (root.key > data) {
			root.left = insertData(root.left, data);
		} else {
			root.right = insertData(root.right, data);
		}
		return root;
	}
	
	private Node search(Node root, int data) {
		if (root ==null || root.key == data) {
			return root;
		} else if (root.key > data) {
			return search(root.left, data);
		} else {
			return search(root.right, data);
		}
	}

	public void display() {
		inOrder(root);
		System.out.println(search(root, 21).key);
	}

	public void inOrder(Node root) {
		if (root != null) {
			inOrder(root.left);
			System.out.println(root.key);
			inOrder(root.right);
		}
	}

	public static void main(String[] args) {
		BinarySearch search = new BinarySearch();
		search.insert(10);
		search.insert(21);
		search.insert(8);
		search.insert(6);
		search.insert(5);
		search.insert(3);
		search.insert(156);
		search.display();
	}
}

