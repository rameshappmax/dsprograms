public class BinarySearch {

	public static void main(String[] args) {
		int a[] = {1,2,4,5,6,7,8,9};
		BinarySearch search = new BinarySearch();
		System.out.println(search.search(a,1,0,a.length-1));
	}

	private int search(int[] a,int key, int l, int r) {
		int m = (l+r)/2;
		if(a[m]==key) {
			return m;
		}else if (key > a[m] ){
			return search(a,key,m+1,r);
		}else {
			return search(a,key,l,m-1);
		}
	}
	
}

