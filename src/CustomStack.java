public class CustomStack {

	int top;

	char data[] = new char[100];

	public CustomStack() {
		top = -1;
	}

	public boolean push(char c) {
		if (top > data.length) {
			return false;
		}
		data[++top] = c;
		return true;
	}

	public char pop() {
		char c = data[top];
		top--;
		return c;
	}

	public boolean isEmpty() {
		if (top == -1) {
			return true;
		}
		return false;
	}

	public char peek() {
		return data[top];
	}

	public static void main(String[] args) {
		CustomStack stack = new CustomStack();
		System.out.println(stack.isEmpty());
		stack.push('a');
		stack.push('b');
		stack.push('c');
		System.out.println("Peek-->" + stack.peek());
		System.out.println("Pop--->" + stack.pop());
		System.out.println("Peek-->" + stack.peek());
	}
}

