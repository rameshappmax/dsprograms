import java.util.Iterator;
import java.util.LinkedList;

public class DFSSearch {

	int value;
	LinkedList<Integer> adj[];
	
	public DFSSearch(int v) {
		this.value = v;
		adj= new LinkedList[v];
		for(int i=0;i< v;i++) {
			adj[i] = new LinkedList<Integer>();
		}
	}
	
	public void addEdge(int v, int w) {
		adj[v].add(w);
	}
	
	public void  DFS(int v) {
		boolean visited[] = new boolean[this.value];
		for(int i=0; i< v ; i++) {
			if(visited[i]== false)
			DFSUtil(i,visited);
		}
	}
	
	public void DFSUtil(int v, boolean[] visited) {
		
		visited[v]=true;
		System.out.print(v+" ");
		Iterator<Integer> it=adj[v].listIterator();
		while(it.hasNext()) {
			int n = it.next();
			if(!visited[n]) {
				DFSUtil(n,visited);
			}
		}
	}
	public static void main(String[] args) {
		DFSSearch  dfsSearch = new DFSSearch(4);
		dfsSearch.addEdge(0, 1);
		dfsSearch.addEdge(0, 2);
		dfsSearch.addEdge(1, 2);
		dfsSearch.addEdge(2, 0);
		dfsSearch.addEdge(2, 3);
		dfsSearch.addEdge(3, 3);
		dfsSearch.DFS(1);
	}
	
	

}

