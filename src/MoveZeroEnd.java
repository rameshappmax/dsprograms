package com.ds.programs;

public class MoveZeroEnd {

	public static void main(String[] args) {
		int a[] = {1,0,0,0,0,2,4,5,0};
		int counter = 0;
		int i = 0;
		while(i < a.length-1) {
			if(a[i]!= 0) {
				a[counter++] = a[i];
			}
			i++;
		}
		while(counter < a.length-1) {
			a[counter++] =0;
		}
		for(int s: a) {
			System.out.print(s + " ");
		}
		
		
	}
}

