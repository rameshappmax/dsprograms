package com.ds.programs;

public class StackLinkList {

	StackNode rootElement;

	static class StackNode {
		int data;
		StackNode nextElement;

		public StackNode(int data) {
			this.data = data;
		}
	}

	public void push(int data) {
		StackNode newNode = new StackNode(data);
		if (rootElement == null) {
			rootElement = newNode;
		} else {
			StackNode temp = rootElement;
			rootElement = newNode;
			newNode.nextElement = temp;
		}
		System.out.println("Data Pushed to Stack" + newNode.data);
	}

	public int pop() {
		int data = Integer.MIN_VALUE;
		if (rootElement == null) {
			System.out.println("Stack is empty");
		} else {
			data = rootElement.data;
			rootElement = rootElement.nextElement;
		}
		return data;
	}

	public int peek() {
		int data = Integer.MIN_VALUE;
		if (rootElement == null) {
			System.out.println("Stack is empty");
		} else {
			data = rootElement.data;
		}
		return data;
	}

	public static void main(String[] args) {
		StackLinkList sll = new StackLinkList();
		sll.push(10);
		sll.push(20);
		sll.push(30);
		System.out.println(sll.pop() + " popped from stack");
		System.out.println("Top element is " + sll.peek());
	}

}

