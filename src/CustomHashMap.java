public class CustomHashMap<K, V> {

	private Entry<K, V>[] buckets;
	private static int initialCapacity = 12;
	int size = 0;

	public CustomHashMap() {
		this(initialCapacity);
	}

	@SuppressWarnings("unchecked")
	public CustomHashMap(int size) {
		this.buckets = new Entry[size];
	}

	public void put(K key, V value) {
		Entry<K, V> entry = new Entry(key, value, null);
		int index = hash(key);
		Entry<K, V> exist = buckets[index];
		if (exist == null) {
			buckets[index] = entry;
			return;
		} else {
			while (exist != null) {
				if (exist.key.equals(key)) {
					exist.value = value;
					return;
				} else {
					exist = exist.next;
				}

			}
			if (exist.key.equals(key)) {
				exist.value = value;

			} else {
				exist.next = entry;
				 size++;
			}
		}
		return;
	}

	public V get(K key) {
		int index = hash(key);
		Entry<K, V> exist = buckets[index];
		while (exist != null) {
			if (exist.key.equals(key)) {
				return exist.value;
			} else {
				exist = exist.next;
			}
		}

		return null;
	}

	public boolean remove(K key) {
		int index = hash(key);

		Entry<K, V> exist = buckets[index];

		if (exist == null) {
			return false;
		}

		Entry<K, V> previous = null;
		while (exist != null) {
			if (exist.key.equals(key)) {
				if (previous == null) {
					buckets[index] = buckets[index].next;
					return true;
				} else {
					previous.next = exist.next;
					return true;
				}
			}
			previous = exist;
			exist = exist.next;
		}
		return false;
	}
	
	public void display(){
		 for(int i=0;i<initialCapacity;i++){
	           if(buckets[i]!=null){
	                  Entry<K, V> entry=buckets[i];
	                  while(entry!=null){
	                        System.out.print("{"+entry.key+"="+entry.value+"}" +" ");
	                        entry=entry.next;
	                  }
	           }
	       }     
	}

	private int hash(K key) {
		return Math.abs(key.hashCode()) % initialCapacity;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		CustomHashMap map = new CustomHashMap<>();
		map.put("1", "Rameshkumar");
		map.put("2", "Senthilkumar");
		map.display();
		System.out.println("\n"+map.get("1"));
		System.out.println(map.remove("3"));

	}

	public class Entry<K, V> {

		final K key;
		V value;
		Entry<K, V> next;

		public Entry(K key, V value, Entry<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

}

