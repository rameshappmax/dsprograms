package com.ds.programs;

public class Node {

	int key;
	Node left;
	Node right;

	Node(int key) {
		this.key = key;
		this.left = this.right = null;

	}

}
