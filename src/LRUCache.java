package com.ds.programs;

import java.util.Map;
import java.util.LinkedHashMap;

@SuppressWarnings("rawtypes")
public final class LRUCache extends LinkedHashMap {
	private static final long serialVersionUID = 1L;
	private int maxSize;
	 private LinkedHashMap<Integer, Integer> map;

	public LRUCache(final int maxSize) {
		this.maxSize = maxSize;
		map = new LinkedHashMap<Integer, Integer>(this.maxSize, 0.75f, true) {
			private static final long serialVersionUID = 1L;

			protected boolean removeEldestEntry(Map.Entry eldest)
            {
                return size() > maxSize;
            }
        };
	}

	
	public void put(int key, int value) {
		map.put(key, value);
	}
	
	public int get(Integer key) {
		return map.getOrDefault(key, -1);
	}
	
	public void display() {
		System.out.println(map);
	}
	
	public static void main(String[] args) {
		LRUCache cache= new LRUCache(2);
		cache.put(1, 1);
		cache.put(2, 2);
		cache.put(3, 3);
		cache.put(4,4);
		cache.display();
		
	}
}
