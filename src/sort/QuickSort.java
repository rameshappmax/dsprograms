package com.ds.programs;

//best,average- nlogn , worst -> n^2

public class QuickSort {

	public static void main(String[] args) {
		int a[] = { 1, 5,0, 8, 3, 2, 9 };
		QuickSort obj = new QuickSort();
		obj.quickSort(a, 0, a.length - 1);
		for(int i :a) {
			System.out.println(i);
		}
	}

	void quickSort(int[] a, int low, int high) {

		if (low < high) {
			int pi = partition(a, low, high);
			quickSort(a, low, pi-1);
			quickSort(a, pi+1, high);
			
		}
	}

	private int partition(int[] a, int low, int high) {

		int pivot = a[high];
		int i = low - 1;
		for (int j = low; j < high; j++) {
			if (a[j] <= pivot) {
				i++;
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		int temp = a[i + 1];
		a[i + 1] = a[high];
		a[high] = temp;

		return i + 1;
	}
}

