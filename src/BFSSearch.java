import java.util.Iterator;
import java.util.LinkedList;

public class BFSSearch {

	int value;

	LinkedList<Integer> adj[];

	BFSSearch(int value) {
		this.value = value;
		this.adj = new LinkedList[value];
		for (int i = 0; i < value; i++) {
			adj[i] = new LinkedList<Integer>();
		}
	}

	private void addEdge(int v, int m) {
		adj[v].add(m);
	}

	private void BFSSearch(int s) {
		boolean visited[] = new boolean[this.value];
		LinkedList<Integer> queue = new LinkedList<Integer>();
		visited[s] = true;
		queue.add(s);
		while (queue.size() != 0) {
			s = queue.poll();
			System.out.print(s + " ");
			Iterator<Integer> it = adj[s].listIterator();
			while (it.hasNext()) {
				int n = it.next();
				if (!visited[n]) {
					visited[n] = true;
					queue.add(n);
				}
			}
		}
	}

	public static void main(String[] args) {
		BFSSearch binarySearch = new BFSSearch(4);
		binarySearch.addEdge(0, 1);
		binarySearch.addEdge(0, 2);
		binarySearch.addEdge(1, 2);
		binarySearch.addEdge(2, 0);
		binarySearch.addEdge(2, 3);
		binarySearch.addEdge(3, 3);
		binarySearch.BFSSearch(1);
	}
}

