public class FindPrimeNumbers {

	public static void main(String[] args) {
		int[] a = {1,2,3,4,5,6,7,8,9,10};
		for(int i=0; i< a.length; i++) {
			if(isPrime(a[i])) {
				System.out.print(a[i]+" ");
			}
		}
		
	}
	
	private static boolean isPrime(int a) {
		for(int i= 2; i <= Math.sqrt(a); i++) {
			if(a%i ==0) {
				return false;
			}
		}
		return true;
	}
}

