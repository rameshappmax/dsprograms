package com.ds.programs;

import java.util.LinkedList;

public class Graph {
	
	int value;
	
	LinkedList<Integer> adj[];
	
	Graph(int value){
		this.value = value;
		this.adj = new LinkedList[value];
		for(int i=0; i < value ; i++) {
			adj[i] = new LinkedList<Integer>();
		}
	}

}

