import java.util.Arrays;

public class CustomArrayList {
	private Object[] dataStore;
	private int actualSize = 0;

	public CustomArrayList() {
		dataStore = new Object[10];
	}

	public Object get(int index) {
		if (index <= actualSize) {
			return dataStore[index];
		} else {
			System.out.println("Out of index");
		}
		return null;
	}

	public boolean add(Object data) {
		if (dataStore.length - actualSize < 5) {
			increaseSize(dataStore);
		}
		dataStore[actualSize++] = data;
		return true;
	}

	public Object remove(int index) {
		if (index <= actualSize) {
			Object obj = dataStore[index];
			dataStore[index] = null;
			int temp = index;
			while (temp < actualSize) {
				dataStore[temp] = dataStore[temp + 1];
				dataStore[temp + 1] = null;
				temp++;
			}
			actualSize--;
			return obj;
		} else {
			System.out.println("Out of index");
		}
		return null;

	}

	private void increaseSize(Object[] dataStore) {
		dataStore = Arrays.copyOf(dataStore, dataStore.length * 2);
	}

	private int size() {
		return actualSize;
	}

	public static void main(String a[]) {
		CustomArrayList mal = new CustomArrayList();
		mal.add(new Integer(2));
		mal.add(new Integer(5));
		mal.add(new Integer(1));
		mal.add(new Integer(23));
		mal.add(new Integer(14));
		for (int i = 0; i < mal.size(); i++) {
			System.out.print(mal.get(i) + " ");
		}
		mal.add(new Integer(29));
		System.out.println("Element at Index 5:" + mal.get(5));
		System.out.println("List size: " + mal.size());
		System.out.println("Removing element at index 2: " + mal.remove(2));
		for (int i = 0; i < mal.size(); i++) {
			System.out.print(mal.get(i) + " ");
		}
	}
}

