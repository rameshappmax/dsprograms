import java.util.NoSuchElementException;

public class CustomHashSet<T> {
	
	public class Node<T> {
	    T data;
	    Node next;

	    Node(T data) {
	        this.data = data;
	        this.next = null;
	    }
	}
	
    private static final Integer INITIAL_CAPACITY = 1 << 4; // 16

    private Node<T>[] buckets;

    private int size;

    public CustomHashSet(final int capacity) {
        this.buckets = new Node[capacity];
        this.size = 0;
    }

    public CustomHashSet() {
        this(INITIAL_CAPACITY);
    }

    public void add(T t) {
        int index = t.hashCode() % buckets.length;

        Node bucket = buckets[index];

        Node<T> newNode = new Node<>(t);

        if (bucket == null) {
            buckets[index] = newNode;
            size++;
            return;
        }

        while (bucket != null) {
            if (bucket.data.equals(t)) {
                return;
            }
            bucket = bucket.next;
        }

        // add only if last element doesn't have the value being added
        if (!bucket.data.equals(t)) {
            bucket.next = newNode;
            size++;
        }
    }
    
    public T remove(T t) {
        int index = t.hashCode() % buckets.length;

        Node bucket = buckets[index];

        if (bucket == null) {
            throw new NoSuchElementException("No Element Found");
        }

        if (bucket.data.equals(t)) {
            buckets[index] = bucket.next;
            size--;
            return t;
        }

        Node temp = bucket;

        while (bucket != null) {
            if (bucket.data.equals(t)) {
            	temp.next = bucket.next;
                size--;
                return t;
            }
            temp = bucket;
            bucket = bucket.next;
        }
        return null;
    }
    
    public static void main(String[] args) {
    	 CustomHashSet<Integer> set = new CustomHashSet<>();
    	 set.add(1);
    	 set.add(2);
    	 set.add(3);
    	 set.add(4);
    	 System.out.println("Sizeee --> " + set.size);
    	 set.remove(4);
    	 System.out.println("Sizeee --> " + set.size);
	}
    
	
}

