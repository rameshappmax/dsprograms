package com.ds.programs;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Java8FlatMap {

	public static void main(String[] args) {
		System.out.println(Stream.of(Arrays.asList(2,3,5),Arrays.asList(5,6,7,9),Arrays.asList(1,5,7,9))
		.flatMap(List::stream).distinct().collect(Collectors.toList()));
	}
}

